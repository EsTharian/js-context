import Store from './Store.mjs';
import Basket from './Basket.mjs';
import Checkout from './Checkout.mjs';
import Header from './Header.mjs';

const store = new Store();

store.subscribe(Basket);
store.subscribe(Checkout);
store.subscribe(Header);

const basket = new Basket();
const header = new Header();
const checkout = new Checkout();

basket.basket = [12, 23, 41, 876];

basket.deleteItems([12, 23]);

console.log(header.quantity);
console.log(checkout.basket);
